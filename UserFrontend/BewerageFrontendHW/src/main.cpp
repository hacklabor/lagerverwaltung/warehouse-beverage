#include <Arduino.h>

#include "SPI.h"
#include <ping.h>
#include <ESP32Ping.h>
#include <Wire.h>

#include "Adafruit_LEDBackpack.h"
#include <Tone32.h>
#include <pitches.h>
#include <Adafruit_NeoPixel.h>
#include <WiFi.h>
#include <HTTPClient.h>

#include <MFRC522.h>
#include "SerialDebug.h"
#include <ArduinoJson.h>
#include "secrets.h"

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 13
// On Trinket or Gemma, suggest changing this to 1

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 3 // Popular NeoPixel ring size

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);
DynamicJsonDocument JsonData(8192);
//Sound
#define SoundCoinUp 1
#define SoundClick 2
#define SoundWifiUp 3
#define SoundLogout 4
#define SoundScanDone 5
//**************************
#define debug 1
#define SS_PIN 5
#define RST_PIN 27
#define AM_PWM_PIN 15
#define CashButton 14
#define SoundPin 12

Adafruit_AlphaNum4 alpha4_1 = Adafruit_AlphaNum4();
Adafruit_AlphaNum4 alpha4_2 = Adafruit_AlphaNum4();
// Init array that will store new NUID
byte nuidPICC[4];
uint16_t GetraenkePWM[] = {0, 40, 200, 360, 510, 590, 670, 750, 830, 870, 910, 950, 1000, 1023};
uint16_t oldPWM = 0;
uint32_t userId = 0;
String UserName = "";
unsigned long LastLogin = 0;
unsigned long LastLogout = 0;
unsigned long LastScrollTime = 0;
boolean Login = false;
int coins = 0;
String ScannerString = "";
boolean StartSignPassed = false;
String UserID = "";
char te[100];
int zeit[8] = {35, 40, 45, 50, 55, 60, 65, 70};
int scroll = 0;

int freq = 2000;
int channel = 15;
int resolution = 8;

struct GetDB
{
  int code;
  String payload;
};

GetDB GetData;
SerialDebug SDebug(ALL);

/*******************************************************************************
CODE
********************************************************************************/
void setBothLED(int r, int g, int b)
{
 
  pixels.setPixelColor(0, 0, 0, 0);
  pixels.setPixelColor(1, r, g, b);
  pixels.setPixelColor(2, r, g, b);
  pixels.show();
  digitalWrite(PIN, LOW);
}
void StartScan()
{

  byte message[] = {0x7E, 0x00, 0x08, 0x01, 0x00, 0x02, 0x01, 0xAB, 0xCD};
  Serial2.write(message, sizeof(message));
  delay(500);
  Serial2.write(message, sizeof(message));
}
void StopScan()
{

  byte message[] = {0x7E, 0x00, 0x08, 0x01, 0x00, 0x02, 0x00, 0xAB, 0xCD};

  Serial2.write(message, sizeof(message));
  delay(500);
  Serial2.write(message, sizeof(message));
}

void ShowBeverageCount(uint8_t count, uint8_t speed)
{

  if (count > 12)
  {
    count = 13;
  }

  if (oldPWM == GetraenkePWM[count])
  {
    return;
  }
  signed int off = (GetraenkePWM[count] - oldPWM) / 10;

  for (int i = 0; i < 10; i++)
  {
    ledcWrite(0, oldPWM + off);

    oldPWM += off;
    delay(speed);
  }
  ledcWrite(0, GetraenkePWM[count]);
  oldPWM = GetraenkePWM[count];
}

uint64_t GetRFID()


{  MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
    MFRC522::MIFARE_Key key;
for (byte i = 0; i < 6; i++)
  {
    key.keyByte[i] = 0xFF;
  }
  delay(20);

   rfid.PCD_Init(); // Init MFRC522

  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if (!rfid.PICC_IsNewCardPresent())
    return 0;
  SDebug.print("RFID", "NEW CARD FOUND", 1);
  // Verify if the NUID has been readed
  if (!rfid.PICC_ReadCardSerial())
    return 0;

  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K)
  {

    return 0;
  }
  uint32_t UID = rfid.uid.uidByte[0];

  UID = UID | rfid.uid.uidByte[1] << 8;
  UID = UID | rfid.uid.uidByte[2] << 16;
  UID = UID | rfid.uid.uidByte[3] << 24;
  SDebug.print("RFID UID", String(UID), 1);

  // Halt PICC
  rfid.PICC_HaltA();

  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
  return UID;
}

void TonePlay(int Sound)
{
  switch (Sound)
  {
  case SoundCoinUp:
    // statements
    tone(SoundPin, NOTE_B5, 100, 1);
    tone(SoundPin, 1);
    tone(SoundPin, NOTE_E6, 300, 1);
    tone(SoundPin, 1);
    break;
  case SoundClick:
    tone(SoundPin, NOTE_E6, 3, 1);
    tone(SoundPin, 1);

    break;
  default:
    // statements
    break;
  }
}
void flashLED(int count, int delayTime, int r1, int g1, int b1, int r2, int g2, int b2)
{
  for (int i = 0; i < count; i++)
  {
    setBothLED(r1, g1, b1);
    delay(delayTime);
    setBothLED(r2, g2, b2);
    delay(delayTime);
  }
}

void serialFlush()
{

  while (Serial2.available() > 0)
  {
    char t = Serial2.read();
      SDebug.print ("flush",String(t),1);
  }
}

char zeichen(int zaehler, char zeichen)
{

  if (zaehler <= zeichen)
  {
    return zaehler;
  }

  return zeichen;
}
char randomchar(int zaehler, int digit)
{
  int rnd = random(32, 126);
  if (rnd == te[digit])
  {

    zeit[digit] = 0;
  }
  if (zaehler < zeit[digit])
  {
    return rnd;
  }
  //TonePlay(SoundCoinUp);
  return te[digit];
}

void randomDisplay()
{
  for (int t = 0; t < 8; t++)
  {
    zeit[t] = 35 + t * 10;
  }

  for (int i = 0; i < 80 + 36; i++)
  {
    TonePlay(SoundClick);
    alpha4_1.writeDigitAscii(0, randomchar(i, 0));
    alpha4_1.writeDigitAscii(1, randomchar(i, 1));
    alpha4_1.writeDigitAscii(2, randomchar(i, 2));
    alpha4_1.writeDigitAscii(3, randomchar(i, 3));
    alpha4_2.writeDigitAscii(0, randomchar(i, 4));
    alpha4_2.writeDigitAscii(1, randomchar(i, 5));
    alpha4_2.writeDigitAscii(2, randomchar(i, 6));
    alpha4_2.writeDigitAscii(3, randomchar(i, 7));
    alpha4_1.writeDisplay();
    alpha4_2.writeDisplay();
    int ze = 0;
    for (int t = 0; t < 8; t++)
    {
      ze += zeit[t];
    }
    if (ze == 0)
    {
      break;
    }

    delay(3);
  }
}

void writeDisplayBuffer(String Name)

{

  if (Name.length() < 9)
  {
    for (int i = sizeof(Name); i < 9; i++)
    {
      Name += " ";
    }
  }
  Name.toCharArray(te, sizeof(Name));
  alpha4_1.writeDigitAscii(0, te[0]);
  alpha4_1.writeDigitAscii(1, te[1]);
  alpha4_1.writeDigitAscii(2, te[2]);
  alpha4_1.writeDigitAscii(3, te[3]);
  alpha4_2.writeDigitAscii(0, te[4]);
  alpha4_2.writeDigitAscii(1, te[5]);
  alpha4_2.writeDigitAscii(2, te[6]);
  alpha4_2.writeDigitAscii(3, te[7]);

  alpha4_1.writeDisplay();
  alpha4_2.writeDisplay();
}
void scrollText(String Text)
{
  String txt = "";
  if (scroll + 8 > Text.length())
  {

    scroll = 0;
  }
  txt = Text.substring(scroll);

  writeDisplayBuffer(txt);
  scroll++;
}

void login()
{

  LastLogin = millis();

  Login = true;
  ShowBeverageCount(coins, 100);
  if (coins > 0)
  {

    StartScan();
    delay(50);
    serialFlush();
  }
  else
  {
    writeDisplayBuffer("           ");
    writeDisplayBuffer("no Coins");
    flashLED(3, 500, 255, 0, 0, 0, 0, 0);
    setBothLED(255, 0, 0);
    randomDisplay();
  }
}
void logout()
{
  writeDisplayBuffer("         ");
  GetData.code = 0;
  GetData.payload = "";
  setBothLED(0, 0, 40);
  StopScan();
  UserID = "";

  LastLogout = millis();
  Login = false;
}

void checkWIFI()
{
  int restart = 5;

  //if (WiFi.status()==WL_CONNECTED) { return; }

  while (WiFi.status() != WL_CONNECTED)
  { // Wait for the Wi-Fi to connect
    if (restart < 5)
    {
      writeDisplayBuffer("WIFI CHK");
      setBothLED(0, 0, 0);
      delay(500);
      Serial.print('.');
      setBothLED(255, 0, 0);
      delay(50);
    }

    restart -= 1;
    if (restart == 0)
    {
       writeDisplayBuffer("        ");
      ESP.restart();
    }
  }
  if (restart < 5)
  {
    writeDisplayBuffer("        ");
  }
}

boolean GetScannerResult()
{
   SDebug.print ("getScanner","",1);
  if (Serial2.available())
  {

    char inChar = (char)Serial2.read();
    Serial.println(inChar);
    if (StartSignPassed)
    {
      if (inChar == '\r')
      {
        Serial.println(ScannerString);
        StartSignPassed = false;
        return true;
      }
      ScannerString += inChar;
    }
    if (inChar == '%')
    {
      StartSignPassed = true;
    }
    return false;
  }
  else
  {
    return false;
  }
}

void GET_DATA(String URL)
{
  HTTPClient http;
  Serial.println("GET: " + URL);
  http.begin(URL, root_ca);  //Specify request destination
  int httpCode = http.GET(); //Send the request
  if (httpCode < 1)
  {
    Serial.println(http.errorToString(httpCode));
  }

  String payload = http.getString(); //Get the response payload

  Serial.println(httpCode); //Print HTTP return code
  Serial.println(payload);  //Print request response payload
  GetData.code = httpCode;
  GetData.payload = payload;
  http.end(); //Close
}
void POST_DATA(String URL, String Daten)
{
  HTTPClient http;
  Serial.println("POST: " + URL);
  Serial.println("payload: " + Daten);
  http.begin(URL, root_ca); //Specify request destination
  http.addHeader("Content-Type", "application/json");
  int httpCode = http.POST(Daten); //Send the request
  if (httpCode < 1)
  {
    Serial.println(http.errorToString(httpCode));
  }
  Serial.println(httpCode); //Print HTTP return code
  GetData.code = httpCode;

  if (httpCode != 204)
  {
    String payload = http.getString(); //Get the response payload
    GetData.payload = payload;
    Serial.println(payload);
  }

  http.end(); //Close
}

void handleProduct()
{
  String DAT = "";
  GET_DATA(String(BackendServer) + "items/" + ScannerString);
  deserializeJson(JsonData, GetData.payload);
  String ProductName = JsonData["data"]["name"];
  ;

  if (GetData.code == 404)
  {
    ProductName = "New Getraenk";
    DAT = "{\"id\":" + ScannerString + ",\"imageUrl\" : \"https://hacklabor.de/assets/img/logo/Logo_medium_black.svg\",\"replacementUrl\" : \"http://Hacklabor.de\",\"name\" : \"Getr�nke\",\"itemType\" : \"REPLACEABLE\",\"description\" : \"20\"}";
    POST_DATA(String(BackendServer) + "items/", DAT);
    if (GetData.code == 201)
    {
      DAT = "{\"id\":" + ScannerString + ",\"column\" : 0,\"row\" : 0,\"shelfId\" : 0	}";
      POST_DATA(String(BackendServer) + "storageUnits/", DAT);
      if ((GetData.code == 409) | (GetData.code == 201))
      {
        DAT = "{\"itemId\":" + ScannerString + "}";
        POST_DATA(String(BackendServer) + "storageUnits/" + ScannerString + "/items", DAT);
      }
    }
  }
  writeDisplayBuffer(ProductName);

  if (ScannerString != "1000000")
  {
    DAT = "{\"itemCount\": 1,\"itemId\":" + ScannerString + ",\"storageUnitId\":" + ScannerString + "}";
    POST_DATA(String(BackendServer) + "users/" + UserID + "/borrow/", DAT);
    DAT = "{\"itemCount\": 1,\"itemId\":1000000,\"storageUnitId\": 1000000}";
    POST_DATA(String(BackendServer) + "users/" + UserID + "/borrow/", DAT);
    if (GetData.code == 204)
    {
      coins -= 1;
      ShowBeverageCount(coins, 5);
      flashLED(5, 100, 0, 255, 0, 0, 0, 0);
      ScannerString = "";
      logout();
    }
    else
    {
      flashLED(20, 50, 255, 0, 0, 0, 0, 0);
      ScannerString = "";
      logout();
    }
  }
  else
  {

    DAT = "{\"itemCount\": 4,\"itemId\":1000000,\"storageUnitId\": 1000000}";
    POST_DATA(String(BackendServer) + "users/" + UserID + "/supply/", DAT);
    if (GetData.code == 204)
    {
      coins += 4;
      writeDisplayBuffer("+4 COINS");
      TonePlay(SoundCoinUp);
      ShowBeverageCount(coins, 5);

      flashLED(5, 100, 0, 255, 0, 0, 0, 0);
      ScannerString = "";

      logout();
    }
    else
    {
      flashLED(20, 50, 255, 0, 0, 0, 0, 0);
      ScannerString = "";
      logout();
    }
  }
}

void setup()
{

  delay(100);
  pinMode(CashButton, INPUT_PULLUP);
  ledcAttachPin(AM_PWM_PIN, 0);
  ledcSetup(0, 4000, 10);
  alpha4_1.begin(0x71); // pass in the address
  alpha4_2.begin(0x72); // pass in the address
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):

  LastLogin = millis();
  LastLogout = millis();
  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels.clear();
  pixels.show();
  Serial.begin(115200);
  Serial2.begin(9600);
  SPI.begin();     // Init SPI bus
 

  
  WiFi.begin(SECRET_SSID, SECRET_PASS);
  SDebug.print("Connecting to", String(SECRET_SSID), 1);

  delay(300);
  checkWIFI();

  Serial.println('\n');
  Serial.println("Connection established!");
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());
  setBothLED(0, 0, 40);

  ScannerString.reserve(200);
  serialFlush();

  TonePlay(SoundCoinUp);
}
void loop()
{

  checkWIFI();

  // Play coin sound

  uint32_t UID = 0;
  if (Login == true)
  {

    if (GetScannerResult())
    {
      SDebug.print("Scan: ", ScannerString, 1);
      handleProduct();
    }

    if (!digitalRead(CashButton))
    {
      delay(50);
      if (!digitalRead(CashButton))
      {
        ScannerString = "1000000";
        setBothLED(255, 255, 255);
        handleProduct();
        while (!digitalRead(CashButton))
        {
        }
      }
    }
  }
  if (Login == false)
  {
    UID = GetRFID();
    if (!digitalRead(CashButton))
    {
      long Testzeit = 0;
      while (!digitalRead(CashButton))
      {
        delay(50);
        Testzeit++;
        if (Testzeit > 20)
        {
          writeDisplayBuffer("           ");
          long rssi = WiFi.RSSI();

          SDebug.print("RSSI", String(rssi), 1);
          writeDisplayBuffer(String(rssi));
        }
      }
      writeDisplayBuffer("           ");
      if (digitalRead(CashButton))
      {
        UID = 1234567890;
      }
    }
    if (UID > 0)
    {
      setBothLED(255, 255, 0);
      setBothLED(255, 255, 0);
      GET_DATA(String(BackendServer) + "users/rfidChipId/" + String(UID));
      if (GetData.code == 200)
      {
        deserializeJson(JsonData, GetData.payload);
        setBothLED(0, 255, 0);
        String count = JsonData["data"]["inventory"]["1000000"]["count"];
        String TMP = JsonData["data"]["id"];
        UserID = TMP;
        String TMP1 = JsonData["data"]["userName"];
        UserName = TMP1;

        writeDisplayBuffer("           ");
        UserName.toCharArray(te, 9);
        randomDisplay();

        coins = count.toInt();
        SDebug.print("Coins", String(coins), 1);
        login();
      }
      else
      {
        flashLED(5, 100, 255, 0, 0, 0, 0, 255);
        setBothLED(0, 0, 40);
      }
    }
  }

  if ((millis() - LastLogin > 6000) & (Login == true))
  {

    logout();
    setBothLED(0, 0, 40);
  }
  if ((millis() - LastLogout > 3000) & (Login == false))
  {
    coins = 0;
    ShowBeverageCount(coins, 100);
    
  }
  if ((millis() - LastScrollTime > 400) & (Login == false))
  {
    scrollText("ZAPFSAEULE                   MANDARIN MEDIEN                   PLANET IC                 UNIKAT MEDIA               TGZ            HACKLABOR.DE                                         ");
    LastScrollTime = millis();
  }
 
  delay(10);
}
